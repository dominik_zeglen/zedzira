#include "string"
#include "vector"
#include "iostream"

#include "Client.hh"
#include "Bill.hh"

using namespace std;

Client::Client()
{
	this->name = "No client name";
}

Client::Client(string name):Nameable(name)
{
	;
}

int Client::getBills()
{
	return this->bills.size();
}

Bill Client::getBill(int n)
{
	return this->bills[n];
}

bool Client::addBill(Bill bill)
{
	this->bills.push_back(bill);
	
	return true;
}

void Client::print()
{
	cout << this->name << ", has paid " << this->bills.size() << " bills\n";
}
