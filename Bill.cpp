#include "vector"
#include "iostream"

#include "Bill.hh"
#include "Item.hh"

using namespace std;

void Bill::print()
{
	cout << "Zawartość rachunku:\n";
	
	for(int i = 0; i < this->content.size(); i++)
		cout << i + 1 << ".\t" << this->content[i].getName() << "\t" << this->content[i].getPrice() << "\n";
	
	cout << "Razem do zaplaty: " << this->getPrice() << " zlociszy\n";
}

int Bill::getSize()
{
	return this->content.size();
}

double Bill::getPrice()
{
	double a;
	a = 0;
	for(int i = 0; i < this->content.size(); i++)
		a += this->content[i].getPrice();
	
	return a;
}

bool Bill::addItem(Item item)
{
	this->content.push_back(item);
	
	return true;
}

bool Bill::removeItem(int x)
{
	this->content.erase(this->content.begin() + x);
	
	return true;
}
