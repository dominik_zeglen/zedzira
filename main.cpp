#include "iostream"

#include "app.hh"

using namespace std;

int main()
{
	App app;
	app.run();
	
	return 0;
}
