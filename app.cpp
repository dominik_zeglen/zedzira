#include "string"

#include "app.hh"

using namespace std;

bool App::run()
{
	while(true)
	{
		cout << "Aktualny klient: " << this->clients[this->actualClientID].getName() << "\n";
		cout << "1. Wybierz klienta.\n2. Przejdz do zakupow.\n3. Przejrzyj rachunki.\n4. Wyjdz i zabij sie\n";
		
		int command;
		cin >> command;
		
		switch(command)
		{
			case 1:
				cout << "Wpisz ID klienta: ";
				int id;
				cin >> id;
				if(id >= this->clients.size())
					cout << "Nieprawidlowe ID\n";
				else
					this->actualClientID = id;
				break;
			
			case 2:
				this->shop();
				break;
			
			case 3:
				for(int i = 0; i < this->clients[this->actualClientID].getBills(); i++)
					this->clients[this->actualClientID].getBill(i).print();
				break;
			
			case 4:
				cout << "No to elo\n";
				return 0;
		}
	}
}

bool App::shop()
{	
		// Interfejs sklepu
	cout << "Co by chcial?\n\n";
	for(int i = 0; i < this->store.size(); i++)
	{
		cout << i + 1 << "\t";
		this->store[i].print();
	}
	
	cout << "Wprowadz numery zamawianych produktow. Aby wyjsc wprowadz '0'\n";
	
	int command;
	Bill bill;
	
	while(true)
	{
		cin >> command;
		if(command)
			bill.addItem(this->store[command - 1]);
		else
			break;
	}
	
	// Dodanie nowego rachunku do klienta
	if(bill.getSize())
		this->clients[this->actualClientID].addBill(bill);
	return true;
}

App::App()
{
	// init
	this->actualClientID = 0;
	this->clients.push_back(Client("Majkel Cwikla"));
	this->clients.push_back(Client("Jan Pawel II"));
	
	this->store.push_back(Item("Maslo", 1000));
	this->store.push_back(Item("Margaryna", 10));
}
