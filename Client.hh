#ifndef _client_hh_included
#define _client_hh_included

#include "string"
#include "vector"

#include "Bill.hh"

using namespace std;

class Client : public Nameable
{
	private:
		string name;
		vector<Bill> bills;
	
	public:
		Client();
		Client(string);
		
		int getBills();
		Bill getBill(int);
		bool addBill(Bill);
		void print();
};

#endif
