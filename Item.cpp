#include "string"
#include "iostream"

#include "Item.hh"
#include "Nameable.hh"

using namespace std;

Item::Item():Nameable("No name")
{
	this->price = 0;
}

Item::Item(string name, double price):Nameable(name)
{
	this->price = price;
}

double Item::getPrice()
{
	return this->price;
}

void Item::print()
{
	cout << this->name << ", " << this->price << " zlociszy\n";
}
