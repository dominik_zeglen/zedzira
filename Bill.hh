#ifndef _bill_hh_defined
#define _bill_hh_defined

#include "vector"

#include "Item.hh"

using namespace std;

class Bill
{
	private:
		vector<Item> content;
		
	public:
		void print();
		int getSize();
		double getPrice();
		bool addItem(Item);
		bool removeItem(int);
};

#endif
