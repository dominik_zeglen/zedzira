#ifndef _item_hh_included
#define _item_hh_included

#include "string"

#include "Nameable.hh"

using namespace std;

class Item : public Nameable
{
	private:
		double price;
		string name;
		
	public:
		Item();
		Item(string, double);
		double getPrice();
		void print();
};

#endif
