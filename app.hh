#ifndef _app_hh_included
#define _app_hh_included

#include "iostream"
#include "vector"

#include "Item.hh"
#include "Client.hh"

using namespace std;

class App
{
	private:
		double taking;
		vector<Item> store;
		vector<Client> clients;
		int actualClientID;
		
	public:
		App();
		bool run();
		bool shop();
};

#endif
