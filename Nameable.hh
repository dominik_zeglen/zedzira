#ifndef _nameable_hh_included
#define _nameable_hh_included

#include "string"

using namespace std;

class Nameable
{
	private:
		string name;
	
	public:
		Nameable();
		Nameable(string);
		string getName();
		virtual void print() = 0;
};

#endif
